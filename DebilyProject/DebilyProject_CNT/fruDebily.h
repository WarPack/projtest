//---------------------------------------------------------------------------

#ifndef fruDebilyH
#define fruDebilyH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <IWAutherBase.hpp>
#include <IWAutherEvent.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class TfrDebily : public TFrame
{
__published:	// IDE-managed Components
	TLabel *laName;
	TLabel *laSurname;
	TIWAutherEvent *IWAutherEvent1;
	TRectangle *Rectangle1;
private:	// User declarations
public:		// User declarations
	__fastcall TfrDebily(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrDebily *frDebily;
//---------------------------------------------------------------------------
#endif
