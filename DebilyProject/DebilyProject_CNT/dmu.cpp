//----------------------------------------------------------------------------

#pragma hdrstop
#include <stdio.h>
#include <memory>

#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
	FInstanceOwner = true;
}

__fastcall Tdm::~Tdm()
{
	delete FdmDatabaseClient;
}

TdmDatabaseClient* Tdm::GetdmDatabaseClient(void)
{
	if (FdmDatabaseClient == NULL)
	{
		SQLConnection1->Open();
		FdmDatabaseClient = new TdmDatabaseClient(SQLConnection1->DBXConnection, FInstanceOwner);
	}
	return FdmDatabaseClient;
};

void __fastcall Tdm::SQLConnection1AfterConnect(TObject *Sender)
{
	SQLConnection1->Open();
    cdsDebily->Open();
}
//---------------------------------------------------------------------------
