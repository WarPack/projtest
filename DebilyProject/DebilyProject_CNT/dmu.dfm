object dm: Tdm
  OldCreateOrder = False
  Height = 351
  Width = 622
  object SQLConnection1: TSQLConnection
    DriverName = 'DataSnap'
    LoginPrompt = False
    Params.Strings = (
      'DriverUnit=Data.DBXDataSnap'
      'HostName=localhost'
      
        'DriverAssemblyLoader=Borland.Data.TDBXClientDriverLoader,Borland' +
        '.Data.DbxClientDriver,Version=24.0.0.0,Culture=neutral,PublicKey' +
        'Token=91d62ebb5b0d1b1b'
      'Port=211'
      'CommunicationProtocol=tcp/ip'
      'DatasnapContext=datasnap/'
      'Filters={}')
    AfterConnect = SQLConnection1AfterConnect
    Connected = True
    Left = 280
    Top = 40
    UniqueId = '{6D1F273D-56C0-4CD7-88AB-7F2E2ABF6005}'
  end
  object DSProviderConnection1: TDSProviderConnection
    ServerClassName = 'TdmDatabase'
    Connected = True
    SQLConnection = SQLConnection1
    Left = 288
    Top = 136
  end
  object cdsDebily: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspDebily'
    RemoteServer = DSProviderConnection1
    Left = 288
    Top = 232
    object cdsDebilyID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object cdsDebilyDEBILY_NAME: TWideStringField
      FieldName = 'DEBILY_NAME'
      Size = 1020
    end
    object cdsDebilyDEBILY_SURNAME: TWideStringField
      FieldName = 'DEBILY_SURNAME'
      Size = 1020
    end
  end
end
