#ifndef uProxyH
#define uProxyH

#include "Data.DBXCommon.hpp"
#include "System.Classes.hpp"
#include "System.SysUtils.hpp"
#include "Data.DB.hpp"
#include "Data.SqlExpr.hpp"
#include "Data.DBXDBReaders.hpp"
#include "Data.DBXCDSReaders.hpp"

  class TdmDatabaseClient : public TObject
  {
  private:
    TDBXConnection *FDBXConnection;
    bool FInstanceOwner;
    TDBXCommand *FDebilydbConnectionAfterConnectCommand;
    TDBXCommand *FDebilydbConnectionBeforeConnectCommand;
    TDBXCommand *FEchoStringCommand;
    TDBXCommand *FReverseStringCommand;
    TDBXCommand *FDebilyAddCommand;
  public:
    __fastcall TdmDatabaseClient(TDBXConnection *ADBXConnection);
    __fastcall TdmDatabaseClient(TDBXConnection *ADBXConnection, bool AInstanceOwner);
    __fastcall ~TdmDatabaseClient();
    void __fastcall DebilydbConnectionAfterConnect(TJSONObject* Sender);
    void __fastcall DebilydbConnectionBeforeConnect(TJSONObject* Sender);
    System::UnicodeString __fastcall EchoString(System::UnicodeString value);
    System::UnicodeString __fastcall ReverseString(System::UnicodeString value);
    void __fastcall DebilyAdd(System::UnicodeString name, System::UnicodeString surname);
  };

#endif
