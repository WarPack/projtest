//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
#include "fruDebily.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall Tfm::OnFrameClick(TObject *Sender)
{
	TLocateOptions xLO;
	dm->cdsDebily->Locate(dm->cdsDebilyID->FieldName, ((TButton*)Sender)->Tag, xLO);

    laGav->Text = dm->cdsDebilyDEBILY_NAME->Value;

	tc->GotoVisibleTab (tiMenu->Index);
}

void __fastcall Tfm::buClick(TObject *Sender)
{
	dm->cdsDebily->First();
	while (!dm->cdsDebily->Eof)
	{
		TfrDebily *x = new TfrDebily (gl);
		x->Parent = gl;
		x->Align = TAlignLayout::Client;
		x->Name = "gl" + IntToStr(dm->cdsDebilyID->Value);
		x->laName->Text = dm->cdsDebilyDEBILY_NAME->Value;
		x->laSurname->Text = dm->cdsDebilyDEBILY_SURNAME->Value;
		x->Tag = dm->cdsDebilyID->Value;
		x->OnClick = OnFrameClick;

		dm->cdsDebily->Next();
	}

	gl->Height = gl->ItemHeight * gl->ChildrenCount;
	gl->RecalcSize();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{

	gl->ItemWidth = gl->Width;
	gl->ItemHeight = 200;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormResize(TObject *Sender)
{
    gl->ItemWidth = gl->Width;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buAddClick(TObject *Sender)
{
	dm->dmDatabaseClient->DebilyAdd(
		edName->Text,
		edSurname->Text);
	dm->cdsDebily->Refresh();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buRefreshClick(TObject *Sender)
{
    dm->cdsDebily->Open();
	dm->cdsDebily->Refresh();

	//dm->cdsDebily->Edit();
	//dm->cdsDebilyDEBILY_NAME->Value = edGav->Text;
	//dm->cdsDebily->ApplyUpdates(-1);
}
//---------------------------------------------------------------------------

