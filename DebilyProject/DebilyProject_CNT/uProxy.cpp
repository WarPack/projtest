//
// Created by the DataSnap proxy generator.
// 15.01.2018 16:19:38
// 

#include "uProxy.h"

void __fastcall TdmDatabaseClient::DebilydbConnectionAfterConnect(TJSONObject* Sender)
{
  if (FDebilydbConnectionAfterConnectCommand == NULL)
  {
    FDebilydbConnectionAfterConnectCommand = FDBXConnection->CreateCommand();
    FDebilydbConnectionAfterConnectCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FDebilydbConnectionAfterConnectCommand->Text = "TdmDatabase.DebilydbConnectionAfterConnect";
    FDebilydbConnectionAfterConnectCommand->Prepare();
  }
  FDebilydbConnectionAfterConnectCommand->Parameters->Parameter[0]->Value->SetJSONValue(Sender, FInstanceOwner);
  FDebilydbConnectionAfterConnectCommand->ExecuteUpdate();
}

void __fastcall TdmDatabaseClient::DebilydbConnectionBeforeConnect(TJSONObject* Sender)
{
  if (FDebilydbConnectionBeforeConnectCommand == NULL)
  {
    FDebilydbConnectionBeforeConnectCommand = FDBXConnection->CreateCommand();
    FDebilydbConnectionBeforeConnectCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FDebilydbConnectionBeforeConnectCommand->Text = "TdmDatabase.DebilydbConnectionBeforeConnect";
    FDebilydbConnectionBeforeConnectCommand->Prepare();
  }
  FDebilydbConnectionBeforeConnectCommand->Parameters->Parameter[0]->Value->SetJSONValue(Sender, FInstanceOwner);
  FDebilydbConnectionBeforeConnectCommand->ExecuteUpdate();
}

System::UnicodeString __fastcall TdmDatabaseClient::EchoString(System::UnicodeString value)
{
  if (FEchoStringCommand == NULL)
  {
    FEchoStringCommand = FDBXConnection->CreateCommand();
    FEchoStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FEchoStringCommand->Text = "TdmDatabase.EchoString";
    FEchoStringCommand->Prepare();
  }
  FEchoStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
  FEchoStringCommand->ExecuteUpdate();
  System::UnicodeString result = FEchoStringCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdmDatabaseClient::ReverseString(System::UnicodeString value)
{
  if (FReverseStringCommand == NULL)
  {
    FReverseStringCommand = FDBXConnection->CreateCommand();
    FReverseStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FReverseStringCommand->Text = "TdmDatabase.ReverseString";
    FReverseStringCommand->Prepare();
  }
  FReverseStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
  FReverseStringCommand->ExecuteUpdate();
  System::UnicodeString result = FReverseStringCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

void __fastcall TdmDatabaseClient::DebilyAdd(System::UnicodeString name, System::UnicodeString surname)
{
  if (FDebilyAddCommand == NULL)
  {
	FDebilyAddCommand = FDBXConnection->CreateCommand();
	FDebilyAddCommand->CommandType = TDBXCommandTypes_DSServerMethod;
	FDebilyAddCommand->Text = "TdmDatabase.DebilyAdd";
	FDebilyAddCommand->Prepare();
  }
  FDebilyAddCommand->Parameters->Parameter[0]->Value->SetWideString(name);
  FDebilyAddCommand->Parameters->Parameter[1]->Value->SetWideString(surname);
  FDebilyAddCommand->ExecuteUpdate();
}


__fastcall  TdmDatabaseClient::TdmDatabaseClient(TDBXConnection *ADBXConnection)
{
  if (ADBXConnection == NULL)
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
  FDBXConnection = ADBXConnection;
  FInstanceOwner = True;
}


__fastcall  TdmDatabaseClient::TdmDatabaseClient(TDBXConnection *ADBXConnection, bool AInstanceOwner)
{
  if (ADBXConnection == NULL) 
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
  FDBXConnection = ADBXConnection;
  FInstanceOwner = AInstanceOwner;
}


__fastcall  TdmDatabaseClient::~TdmDatabaseClient()
{
  delete FDebilydbConnectionAfterConnectCommand;
  delete FDebilydbConnectionBeforeConnectCommand;
  delete FEchoStringCommand;
  delete FReverseStringCommand;
  delete FDebilyAddCommand;
}

