//----------------------------------------------------------------------------

#ifndef dmuH
#define dmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include "uProxy.h"
#include <Data.DB.hpp>
#include <Data.DBXCommon.hpp>
#include <Data.DBXDataSnap.hpp>
#include <Data.SqlExpr.hpp>
#include <IPPeerClient.hpp>
#include <Datasnap.DBClient.hpp>
#include <Datasnap.DSConnect.hpp>
//----------------------------------------------------------------------------
class Tdm : public TDataModule
{
__published:	// IDE-managed Components
	TSQLConnection *SQLConnection1;
	TDSProviderConnection *DSProviderConnection1;
	TClientDataSet *cdsDebily;
	TIntegerField *cdsDebilyID;
	TWideStringField *cdsDebilyDEBILY_NAME;
	TWideStringField *cdsDebilyDEBILY_SURNAME;
	void __fastcall SQLConnection1AfterConnect(TObject *Sender);
private:	// User declarations
	bool FInstanceOwner;
	TdmDatabaseClient* FdmDatabaseClient;
	TdmDatabaseClient* GetdmDatabaseClient(void);
public:		// User declarations
	__fastcall Tdm(TComponent* Owner);
	__fastcall ~Tdm();
	__property bool InstanceOwner = {read=FInstanceOwner, write=FInstanceOwner};
	__property TdmDatabaseClient* dmDatabaseClient = {read=GetdmDatabaseClient, write=FdmDatabaseClient};
};
//---------------------------------------------------------------------------
extern PACKAGE Tdm *dm;
//---------------------------------------------------------------------------
#endif
