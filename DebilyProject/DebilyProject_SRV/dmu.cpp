//----------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
#include <stdio.h>
#include <memory>
#include "dmuDatabase.h"

#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
}
//----------------------------------------------------------------------------
void __fastcall Tdm::DSServerClass1GetClass(TDSServerClass *DSServerClass,
          TPersistentClass &PersistentClass)
{
	PersistentClass =  __classid(TdmDatabase);
}
//----------------------------------------------------------------------------
void runDSServer()
{
	printf("Starting %ls\n", __classid(Tdm)->ClassName().c_str());
	std::auto_ptr<Tdm> module(new Tdm(NULL));
	module->DSServer1->Start();
	TInputRecord inputRecord;
	DWORD event;
	printf("Press ESC to stop the server");
	HANDLE stdIn = GetStdHandle(STD_INPUT_HANDLE);
	while (true)
	{
		ReadConsoleInput(stdIn, &inputRecord, 1, &event);
		if (inputRecord.EventType == KEY_EVENT &&
		inputRecord.Event.KeyEvent.bKeyDown &&
		inputRecord.Event.KeyEvent.wVirtualKeyCode == VK_ESCAPE)
			break;
	}
	module->DSServer1->Stop();
}
//----------------------------------------------------------------------------
//---------------------------------------------------------------------------

