//---------------------------------------------------------------------------

#ifndef dmuDatabaseH
#define dmuDatabaseH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <DataSnap.DSServer.hpp>
#include <Datasnap.DSProviderDataModuleAdapter.hpp>
#include <Data.DB.hpp>
#include <Data.DBXFirebird.hpp>
#include <Data.FMTBcd.hpp>
#include <Data.SqlExpr.hpp>
#include <Datasnap.Provider.hpp>
//---------------------------------------------------------------------------
class TdmDatabase : public TDSServerModule
{
__published:	// IDE-managed Components
	TSQLConnection *DebilydbConnection;
	TSQLDataSet *taDebily;
	TDataSetProvider *dspDebily;
	TSQLStoredProc *Debily_insProc;
	void __fastcall DebilydbConnectionAfterConnect(TObject *Sender);
	void __fastcall DebilydbConnectionBeforeConnect(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall TdmDatabase(TComponent* Owner);
	System::UnicodeString EchoString(System::UnicodeString value);
	System::UnicodeString  ReverseString(System::UnicodeString value);

    void DebilyAdd (UnicodeString name, UnicodeString surname);
};
//---------------------------------------------------------------------------
extern PACKAGE TdmDatabase *dmDatabase;
//---------------------------------------------------------------------------
#endif

