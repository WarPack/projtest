//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dmuDatabase.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TdmDatabase::TdmDatabase(TComponent* Owner)
	: TDSServerModule(Owner)
{
}
//----------------------------------------------------------------------------
System::UnicodeString TdmDatabase::EchoString(System::UnicodeString value)
{
    return value;
}
//----------------------------------------------------------------------------
System::UnicodeString TdmDatabase::ReverseString(System::UnicodeString value)
{
    return ::ReverseString(value);
}
//----------------------------------------------------------------------------

void __fastcall TdmDatabase::DebilydbConnectionAfterConnect(TObject *Sender)
{
	DebilydbConnection->Open();
	taDebily->Open();
}
//---------------------------------------------------------------------------

void __fastcall TdmDatabase::DebilydbConnectionBeforeConnect(TObject *Sender)
{
    DebilydbConnection->Params->Values["DataBase"] = "..\\..\\..\\Assets\\DEBILBD.FDB";
}
//---------------------------------------------------------------------------

void TdmDatabase::DebilyAdd(UnicodeString name, UnicodeString surname)
{
	Debily_insProc->Params->ParamByName("DEBILY_NAME")->Value = name;
	Debily_insProc->Params->ParamByName("DEBILY_SURNAME")->Value = surname;
    Debily_insProc->ExecProc();
}
