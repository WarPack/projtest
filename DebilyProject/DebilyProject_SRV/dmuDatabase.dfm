object dmDatabase: TdmDatabase
  OldCreateOrder = False
  Height = 398
  Width = 597
  object DebilydbConnection: TSQLConnection
    ConnectionName = 'FBConnection'
    DriverName = 'Firebird'
    LoginPrompt = False
    Params.Strings = (
      'DriverUnit=Data.DBXFirebird'
      
        'DriverPackageLoader=TDBXDynalinkDriverLoader,DbxCommonDriver250.' +
        'bpl'
      
        'DriverAssemblyLoader=Borland.Data.TDBXDynalinkDriverLoader,Borla' +
        'nd.Data.DbxCommonDriver,Version=24.0.0.0,Culture=neutral,PublicK' +
        'eyToken=91d62ebb5b0d1b1b'
      
        'MetaDataPackageLoader=TDBXFirebirdMetaDataCommandFactory,DbxFire' +
        'birdDriver250.bpl'
      
        'MetaDataAssemblyLoader=Borland.Data.TDBXFirebirdMetaDataCommandF' +
        'actory,Borland.Data.DbxFirebirdDriver,Version=24.0.0.0,Culture=n' +
        'eutral,PublicKeyToken=91d62ebb5b0d1b1b'
      'GetDriverFunc=getSQLDriverINTERBASE'
      'LibraryName=dbxfb.dll'
      'LibraryNameOsx=libsqlfb.dylib'
      'VendorLib=fbclient.dll'
      'VendorLibWin64=fbclient.dll'
      'VendorLibOsx=/Library/Frameworks/Firebird.framework/Firebird'
      'Role=RoleName'
      'MaxBlobSize=-1'
      'TrimChar=False'
      'DriverName=Firebird'
      'Database=C:\Users\myrka\Desktop\DebilyProject\Assets\DEBILBD.FDB'
      'RoleName=RoleName'
      'User_Name=sysdba'
      'Password=masterkey'
      'ServerCharSet=UTF8'
      'SQLDialect=3'
      'ErrorResourceFile='
      'LocaleCode=0000'
      'BlobSize=-1'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'IsolationLevel=ReadCommitted'
      'Trim Char=False')
    AfterConnect = DebilydbConnectionAfterConnect
    BeforeConnect = DebilydbConnectionBeforeConnect
    Connected = True
    Left = 215
    Top = 60
  end
  object taDebily: TSQLDataSet
    Active = True
    CommandText = 'DEBILY'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = DebilydbConnection
    Left = 215
    Top = 140
  end
  object dspDebily: TDataSetProvider
    DataSet = taDebily
    Left = 216
    Top = 232
  end
  object Debily_insProc: TSQLStoredProc
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftString
        Precision = 255
        Name = 'DEBILY_NAME'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Precision = 255
        Name = 'DEBILY_SURNAME'
        ParamType = ptInput
      end>
    SQLConnection = DebilydbConnection
    StoredProcName = 'DEBILY_INS'
    Left = 222
    Top = 312
  end
end
