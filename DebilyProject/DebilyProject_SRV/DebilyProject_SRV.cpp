//----------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
#include <stdio.h>
#include <memory>
//----------------------------------------------------------------------------
USEFORM("dmuDatabase.cpp", dmDatabase); /* TDSServerModule: File Type */
USEFORM("dmu.cpp", dm); /* TDataModule: File Type */
//---------------------------------------------------------------------------
extern void runDSServer();
//----------------------------------------------------------------------------
#pragma argsused
int _tmain(int argc, _TCHAR* argv[])
{
  try
  {
    runDSServer();
  }
  catch (Exception &exception)
  {
    printf("%ls: %ls", exception.ClassName().c_str(), exception.Message.c_str());
  }
  return 0;
}
//----------------------------------------------------------------------------

